package main

import (
	"fmt"
	"bitbucket.org/jdnewman/go-mud/pkg/server"
	_ "bitbucket.org/jdnewman/go-mud/pkg/config"
)

const address = ":2222"

func main() {
	fmt.Println("Hello Worlds")

	s, err := server.NewServer()
	if err != nil { panic(err) }
	err = s.ListenAndServe(address)
	if err != nil { panic(err) }
	panic("We should never be here")
}
