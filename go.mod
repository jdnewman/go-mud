module bitbucket.org/jdnewman/go-mud

go 1.16

require (
	code.rocketnine.space/tslocum/cview v1.5.6
	github.com/gdamore/tcell/v2 v2.4.0
	github.com/mattn/go-runewidth v0.0.13
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97
)
