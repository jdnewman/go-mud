package server

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"

	"golang.org/x/crypto/ssh"

	"bitbucket.org/jdnewman/go-mud/pkg/connection"
)

type Server struct {
	address string

	motd string

	sshConfig *ssh.ServerConfig

	listener net.Listener
	connections []*connection.Connection
}

func init() {
	fmt.Printf("server - init()\n")
}

func NewServer() (*Server, error) {
	// TODO config
	sshConfig := &ssh.ServerConfig{
		NoClientAuth: true,
	}
	// TODO don't hardcode file path
	keyBytes, err := ioutil.ReadFile("id_rsa")
	if err != nil {
		return nil, err
	}
	key, err := ssh.ParsePrivateKey(keyBytes)
	if err != nil {
		return nil, err
	}
	sshConfig.AddHostKey(key)

	var s Server
	s.sshConfig = sshConfig

	return &s, nil
}

func (this *Server) ListenAndServe(address string) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	this.address = address
	this.listener = listener

	for {
		conn, err := this.listener.Accept()
		if err != nil {
			fmt.Fprintf(os.Stderr, "WARN: Unable to accept connection: '%s'\n", err)
			continue
		}

		// TODO Factor into server.AddConnection or similar
		c, err := connection.NewConnection(conn, this.sshConfig)
		if err != nil {
			fmt.Fprintf(os.Stderr, "WARN: Unable to create new ssh connection: '%s'\n", err)
			continue
		}
		this.connections = append(this.connections, c)
		go c.Serve()
	}
}

