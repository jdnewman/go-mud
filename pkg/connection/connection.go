package connection

import (
	"encoding/binary"
	"fmt"
	"net"
	"os"

	"golang.org/x/crypto/ssh"

	"github.com/gdamore/tcell/v2"
	"github.com/mattn/go-runewidth"
	//"github.com/rivo/tview"
	"code.rocketnine.space/tslocum/cview"

	"bitbucket.org/jdnewman/go-mud/pkg/sshTty"
)

type Connection struct {
	sshConn *ssh.ServerConn
	newChannels  <-chan ssh.NewChannel
	globalRequests <-chan *ssh.Request

	session *ssh.Channel

	sshTty *sshTty.SshTty
	screen *tcell.Screen
	cviewApp *cview.Application
}

func init() {
	fmt.Printf("connection - init()\n")
}

func NewConnection(conn net.Conn, sshConfig *ssh.ServerConfig) (*Connection, error) {
	sshConn, newChannels, globalRequests, err := ssh.NewServerConn(conn, sshConfig)
	if err != nil {
		fmt.Fprintf(os.Stderr, "WARN: Unable to create new ssh server connection: '%s'\n", err)
		return nil, err
	}
	fmt.Printf("Connected: %s!\n", sshConn.User())

	c := Connection{
		sshConn: sshConn,
		newChannels: newChannels,
		globalRequests: globalRequests,
	}

	return &c, nil
}

func (this *Connection) Serve() {
	go ssh.DiscardRequests(this.globalRequests)

	for newChannel := range this.newChannels {
		this.serviceNewChannel(newChannel)
	}
}

func (this *Connection) SetupTty() {
	this.sshTty = &sshTty.SshTty{
		Channel: *this.session,
		Width: 0, //TODO Implement (Maybe wait until pty-req?)
		Height: 0, //TODO Implement
	}
}

func (this *Connection) ServeTtyRequests(requests <-chan *ssh.Request) {
	for request := range requests {
		switch request.Type {
		case "pty-req":
			termLen := request.Payload[3] //TODO Remove/doc magic
			termEnv := string(request.Payload[4:termLen+4])
			w, h := parseTerminalDimensions(request.Payload[termLen+4:])
			this.sshTty.Width = w
			this.sshTty.Height = h
			fmt.Printf("pty-req '%s'", termEnv)

			this.sshTty.DoNotifyResize()

			request.Reply(true, nil)
			continue
		case "shell":
			request.Reply(true, nil)
			continue
		case "window-change":
			w, h := parseTerminalDimensions(request.Payload)
			fmt.Printf("New window size: %d %d\n", w, h)
			this.sshTty.Width = w
			this.sshTty.Height = h

			this.sshTty.DoNotifyResize()
		}

		request.Reply(false, nil)
	}
}

func (this *Connection) SetupCviewInterface() {
	screen, err := tcell.NewTerminfoScreenFromTty(this.sshTty)
	if err != nil {
		fmt.Printf("ERROR: unable to create screen from tty\n")
		return
	}
	err = screen.Init()
	if err != nil {
		fmt.Printf("ERROR: unable to init screen\n")
		return
	}
	this.screen = &screen

	cview.Styles.ContrastBackgroundColor = tcell.ColorBlue.TrueColor()
	cview.Styles.MoreContrastBackgroundColor = tcell.ColorDarkBlue.TrueColor()

	app := cview.NewApplication()
	app.SetScreen(screen)
	app.EnableMouse(true)


	tv := cview.NewTextView()
	tv.SetText("Blargle bksjdfkj sdf sdf asdf")

	w1 := cview.NewWindow(tv)
	w1.SetRect(100, 0, 40, 10)
	w1.SetTitle("Hi")

	wm := cview.NewWindowManager()
	wm.SetBackgroundTransparent(true)
	wm.Add(w1)

	app.SetRoot(wm, true)

	app.SetBeforeDrawFunc(drawMapFunc)

	this.cviewApp = app
}

func (this *Connection) ServeCviewInterface() {
	err := this.cviewApp.Run()
	if err != nil {
		fmt.Printf("ERROR: unable to create/run cview application\n")
		return
	}
}

func (this *Connection) serviceNewChannel(newChannel ssh.NewChannel) {
	channelType := newChannel.ChannelType()
	if channelType != "session" {
		newChannel.Reject(ssh.UnknownChannelType, "Unkown Channel Type")
		fmt.Fprintf(os.Stderr, "WARN: Rejecting unkown channel type: '%s'\n", channelType)
		return
	}

	session, requests, err := newChannel.Accept()
	if err != nil {
		fmt.Fprintf(os.Stderr, "WARN: Unable to accept new channel: '%s'\n", err)
		return
	}

	this.session = &session

	fmt.Fprintf(session, "Welcome!\r\n")

	this.SetupTty()
	go this.ServeTtyRequests(requests)

	this.SetupCviewInterface()
	go this.ServeCviewInterface()
}

func drawMapFunc(s tcell.Screen) bool {
	s.Clear()

	w, h := s.Size()
	y := h/2
	txt := []byte("[ --- Insert Map Here ---]")
	cview.Print(s, txt, 0, y-1, w, cview.AlignCenter, tcell.ColorRed)

	displayString(s, "[ --- Insert Map Here ---]")

	return false
}

func parseTerminalDimensions(b []byte) (int, int) {
	w := binary.BigEndian.Uint32(b)
	h := binary.BigEndian.Uint32(b[4:])
	return int(w), int(h)
}

func emitStr(s tcell.Screen, x, y int, style tcell.Style, str string) {
	for _, c := range str {
		var comb []rune
		w := runewidth.RuneWidth(c)
		if w == 0 {
			comb = []rune{c}
			c = ' '
			w = 1
		}
		s.SetContent(x, y, c, comb, style)
		x += w
	}
}

func displayString(s tcell.Screen, str string) {
	w, h := s.Size()
	strLen := len(str)
	x := w/2 - strLen/2
	y := h/2

	style := tcell.StyleDefault.Foreground(tcell.ColorBlue).Background(tcell.ColorBlack)
	emitStr(s, x, y, style, str)
}

func drawRune(s tcell.Screen, x, y int, style tcell.Style, r rune) {
	s.SetContent(x, y, r, nil, style)
}
