package sshTty

import (
	"fmt"

	"golang.org/x/crypto/ssh"
)

type SshTty struct {
	ssh.Channel

	Width, Height int
	TtyResizeCb func()
}

func init() {
	fmt.Printf("sshTty - init()\n")
}

func (this *SshTty) Start() error {
	fmt.Printf("sshTty - Start()\n")
	return nil
}

func (this *SshTty) Stop() error {
	fmt.Printf("sshTty - Stop()\n")
	return nil
}

func (this SshTty) Drain() error {
	fmt.Printf("sshTty - Drain()\n")
	return nil
}

func (this *SshTty) NotifyResize(cb func()) {
	fmt.Printf("sshTty - NofityResize() - cb: %p\n", cb)
	this.TtyResizeCb = cb
	return
}

func (this *SshTty) WindowSize() (width, height int, err error) {
	fmt.Printf("sshTty - WindowSize()\n")
	fmt.Printf("\tw: %d, h: %d\n", this.Width, this.Height)

	return this.Width, this.Height, nil
}


func (this *SshTty) DoNotifyResize() {
	fmt.Printf("sshTty - DoNotifyResize()\n")
	fmt.Printf("\tTtyResizeCb: %p\n", this.TtyResizeCb)
	if this.TtyResizeCb != nil {
		this.TtyResizeCb()
	}
}
